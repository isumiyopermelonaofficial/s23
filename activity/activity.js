// Insert Single Room using InsertOne Method
db.rooms.insertOne(
{
   name: "sinlge",
   accomadates: 2,
   price: 1000,
   description: "A simple room with all the basic necessities",
   isAvailable: false 
}
)

// Insert Multiple Rooms using the insertMany Method
db.rooms.insertMany([
{
   name: "double",
   accomadates: 3,
   price: 2000,
   description: "A room fit for a small family going on a vacation",
   rooms_availalbe: 5, 
   isAvailable: false 
},
{
   name: "queen",
   accomadates: 4,
   price: 4000,
   description: "A room with a queen sized bed perfect for a simple getaway",
   rooms_available: 15, 
   isAvailable: false  
}
])

// Using find method to search for a room with the name double
db.rooms.find(
{name: "double"}
)

// Using updateOne method to update the queen rooms to have 0 availability
db.rooms.updateOne(
{name: "queen"},
{
   $set: {
   rooms_available: 0
       }
}
)

// Using deleteMany methods to delete rooms with 0 availability
db.rooms.deleteMany(
{
    rooms_available: 0
}
)